#include "logger.h"

Logger::Logger(string* showString, mutex* stringLock)
{
    output = showString;
    outputLock = stringLock;
}

void Logger::run()
{
    while(1)
    {
        outputLock->lock();
        if (output->length() > 5)
        {
            QString str = QString::fromUtf8(output->c_str());
            qDebug() << "\n\n\n\n\n\n" << str << "\n\n\n\n\n\n\n" << endl;


        }
        outputLock->unlock();
        sleep(1);
    }
}
