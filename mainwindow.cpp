#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QFuture>
#include <QScrollBar>
#include <QtConcurrent>


MainWindow::MainWindow(SocketWrapper* inSocket, atomic<Command_t> *command, string* fileAddress,
                       string* outputString, mutex* outputLock, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    setWindowTitle("IAS0410QtPlantLogger");

    sock = inSocket;
    commandToDo = command;
    this->fileAddress = fileAddress;
    this->outputString = outputString;
    this->outputLock = outputLock;

    //disabe unused buttons
    ui->ClosePushButton->setDisabled(true);
    ui->ClosePushButton->repaint();

    ui->ConnectPushButton->setDisabled(true);
    ui->ConnectPushButton->repaint();

    ui->DisconnectPushButton->setDisabled(true);
    ui->DisconnectPushButton->repaint();

    ui->StartPushButton->setDisabled(true);
    ui->StartPushButton->repaint();

    ui->BreakPushButton->setDisabled(true);
    ui->BreakPushButton->repaint();


    logger = new Logger(outputString, outputLock);
    logger->start();
    //QThread logThread = new QThread()

}

MainWindow::~MainWindow()
{
    delete ui;

}


void MainWindow::on_OpenPushButton_clicked()
{

    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 "/home",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
    qDebug() << dir << endl;
    string address = dir.toUtf8().constData();

    if(address.length()>2)
    {
        *fileAddress = address;

        ui->ClosePushButton->setEnabled(true);
        ui->ClosePushButton->repaint();

        ui->ConnectPushButton->setEnabled(true);
        ui->ConnectPushButton->repaint();

        ui->OpenPushButton->setEnabled(false);
        ui->OpenPushButton->repaint();

        QFuture<void> future = QtConcurrent::run(this, &MainWindow::run);
    }
}

void MainWindow::on_ClosePushButton_clicked()
{
    ui->ClosePushButton->setDisabled(true);
    ui->ClosePushButton->repaint();

    ui->ConnectPushButton->setDisabled(true);
    ui->ConnectPushButton->repaint();

    ui->DisconnectPushButton->setDisabled(true);
    ui->DisconnectPushButton->repaint();

    ui->StartPushButton->setDisabled(true);
    ui->StartPushButton->repaint();

    ui->BreakPushButton->setDisabled(true);
    ui->BreakPushButton->repaint();

    ui->OpenPushButton->setEnabled(true);
    ui->OpenPushButton->repaint();
}

void MainWindow::on_ConnectPushButton_clicked()
{
    if(connectionStatus == FALSE)
    {
        int res = sock->connect();
        if (res >= 0 && GetLastError() != 10061)
        {
            SetEvent(connectsEvent);
            SetEvent(connectEvent);
            connectionStatus = TRUE;
            qDebug() << "connect events are sent" << res << "  " << GetLastError() << endl;

            ui->ClosePushButton->setEnabled(false);
            ui->ClosePushButton->repaint();

            ui->ConnectPushButton->setEnabled(false);
            ui->ConnectPushButton->repaint();

            ui->DisconnectPushButton->setEnabled(true);
            ui->DisconnectPushButton->repaint();

            ui->StartPushButton->setEnabled(true);
            ui->StartPushButton->repaint();

            ui->BreakPushButton->setEnabled(false);
            ui->BreakPushButton->repaint();

            ui->OpenPushButton->setEnabled(false);
            ui->OpenPushButton->repaint();
        }
        else
        {
            qDebug() << "cannot connect to server" << endl;

            QMessageBox messageBox;
            messageBox.critical(0,"Error","Cannot connect to the server. Maybe the emulator is not running.");
            //messageBox.setFixedSize(500,200);
        }
    }
}

void MainWindow::on_ExitPushButton_clicked()
{
    this->close();
}

void MainWindow::on_DisconnectPushButton_clicked()
{
    qDebug() << "clicked" << endl;
    if (connectionStatus == TRUE)
    {
        commandToDo->store(stopCommand);
        cout << "stop" << endl;
        connectionStatus = FALSE;
        SetEvent(commandEvent);

        ui->ClosePushButton->setEnabled(true);
        ui->ClosePushButton->repaint();

        ui->ConnectPushButton->setEnabled(true);
        ui->ConnectPushButton->repaint();

        ui->DisconnectPushButton->setEnabled(false);
        ui->DisconnectPushButton->repaint();

        ui->StartPushButton->setEnabled(false);
        ui->StartPushButton->repaint();

        ui->BreakPushButton->setEnabled(false);
        ui->BreakPushButton->repaint();

        ui->OpenPushButton->setEnabled(false);
        ui->OpenPushButton->repaint();

    }
}

void MainWindow::on_StartPushButton_clicked()
{
    qDebug() << "clicked " << commandToDo->is_lock_free() << endl;
    if (connectionStatus == TRUE)
    {
        if(commandToDo->is_lock_free())
        {
            commandToDo->store(startCommand);
            qDebug() << "start" << endl;
            SetEvent(commandEvent);

            ui->StartPushButton->setEnabled(false);
            ui->StartPushButton->repaint();

            ui->BreakPushButton->setEnabled(true);
            ui->BreakPushButton->repaint();

        }

    }
}

void MainWindow::on_BreakPushButton_clicked()
{
    qDebug() << "clicked" << endl;
    if (connectionStatus == TRUE)
    {
        commandToDo->store(breakCommand);
        cout << "break" << endl;
        SetEvent(commandEvent);

        ui->StartPushButton->setEnabled(true);
        ui->StartPushButton->repaint();

        ui->BreakPushButton->setEnabled(false);
        ui->BreakPushButton->repaint();

    }
}

void MainWindow::on_MainWindow_destroyed()
{
    closed = true;
    qDebug("Closed");
}

void MainWindow::run(void)
{
    while(!closed)
    {
        outputLock->lock();
        if (outputString->length() > 5)
        {
            QString str = QString::fromLocal8Bit(outputString->c_str());
            ui->LogbookTextBrowser->append(str);
            *outputString = string("");
        }
        outputLock->unlock();
        Sleep(300);
    }
}

void MainWindow::on_LogbookTextBrowser_textChanged()
{
    QScrollBar *sb = ui->LogbookTextBrowser->verticalScrollBar();
    sb->setValue(sb->maximum());
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "IAS0410QtPlantLogger",
                                                                    tr("Are you sure?\n"),
                                                                    QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                    QMessageBox::Yes);
        if (resBtn != QMessageBox::Yes) {
            event->ignore();
        } else {
            closed = true;
            event->accept();
        }
}
