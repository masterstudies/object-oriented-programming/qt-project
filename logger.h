#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QThread>

#include <QDebug>
#include <Windows.h>
//#include "mainwindow.h"

using namespace std;

class Logger : public QThread
{

        Q_OBJECT
    private:
        string *output;
        mutex *outputLock;


    public:
        Logger(string* showString, mutex* stringLock);
    protected:
        void run() override;


};

#endif // LOGGER_H
