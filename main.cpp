#include "mainwindow.h"
#include "IAS0410ObjPlantLogger.h"
#include <QApplication>
#include <QThread>

int main(int argc, char *argv[])
{
    atomic<Command_t> command(freeCommand);
    string fileAddress;
    string outputString("");
    mutex outputLock;

    HANDLE exitEvent = CreateEvent(nullptr, TRUE, FALSE, L"EXIT");

    SocketWrapper sock("127.0.0.1", "1234");

    //KeyboardReceiver* kb = new KeyboardReceiver(&sock ,&command);
    CommandSender* cs = new CommandSender(&sock, &command);
    DataReceiverAndAnalyzer analyze(&sock, &fileAddress, &outputString, &outputLock);

    //thread kb_thread(*kb);
    thread sender_thread(&CommandSender::run, cs);
    thread receiver_thread(&DataReceiverAndAnalyzer::run, &analyze);

    QApplication a(argc, argv);
    MainWindow w(&sock, &command, &fileAddress, &outputString, &outputLock);
    w.show();


    a.exec();
    SetEvent(exitEvent);
    sender_thread.join();
    receiver_thread.join();
    return 0;
}
