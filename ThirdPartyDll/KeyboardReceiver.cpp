#include "KeyboardReceiver.h"

KeyboardReceiver::KeyboardReceiver(SocketWrapper* inSocket, atomic<Command_t>* command)
{
	commandToDo = command;
	sock = inSocket;
}


void KeyboardReceiver::operator()()
{
	bool exit = FALSE;
	while (!exit)
	{
		string receivedLine;
		getline(cin, receivedLine);

		if (receivedLine == "exit")
		{
			SetEvent(exitEvent);
			exit = TRUE;
		}
		else if (receivedLine == "connect" && connectionStatus == FALSE)
		{	
			if (sock->connect() >= 0)
			{
				SetEvent(connectsEvent);
				SetEvent(connectEvent);
				connectionStatus = TRUE;
				cout << "connect events are sent" << endl;
			}
			else
			{
				cout << "cannot connect to server" << endl;
			}
			
		}
		else if (receivedLine == "start" && connectionStatus)
		{
			commandToDo->store(startCommand);
			cout << "start" << endl;
			SetEvent(commandEvent);
			
		}
		else if (receivedLine == "stop" && connectionStatus)
		{
			commandToDo->store(stopCommand);
			cout << "stop" << endl;
			connectionStatus = FALSE;
			SetEvent(commandEvent);
			
		}
		else if (receivedLine == "break" && connectionStatus)
		{
			commandToDo->store(breakCommand);
			cout << "break" << endl;
			SetEvent(commandEvent);
			
		}

		cout << "command received =" << receivedLine << endl;
	}
	cout << "exit the keyboard thread" << endl;
}
