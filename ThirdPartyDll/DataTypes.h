#pragma once

typedef struct
{
	double AirInlet = -1;
	double InsideTemperature = -1;
	double GasInlet = -1;
	double GasOutlet = -1;

}ChannelHeater_t;

typedef struct
{
	double SolutionConductivity = -1;
	int SolutionConcentration = -1;
	double SolutionPH = -1;
	double Level = -1;
}ChannelSolutionTank_t;

typedef struct
{
	double Inlet = -1;
	double Outlet = -1;
}ChannelScrubber_t;

