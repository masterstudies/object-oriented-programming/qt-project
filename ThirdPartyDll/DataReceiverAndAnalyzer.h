#pragma once
#include <string.h>
#include <exception>
#include "SocketWrapper.h"
#include "DataTypes.h"
#include <string>
#include <fstream>
#include <algorithm>
#include <mutex>
#include <atomic>

#define ANALYZE_MAX_LEN 2000

using namespace std;

class DataReceiverAndAnalyzer
{
private:
	enum State_t
	{
		initialState,
		readyState,
		disconnectState,
		idleState,
		exitState
	};
	
	
	SocketWrapper* sock;
	string outputString;
    string *outputStringPtr = nullptr;
    mutex* outputLock;

	bool firstLog = true;
        string* logAddress;
	string logFileName;
	string logCompeleteAddress;
	fstream logFile;
	atomic<bool> receivedData;
	
	State_t currentState = disconnectState;

    HANDLE connectEvent = CreateEvent(nullptr, FALSE, FALSE, L"CONNECTION");
    HANDLE passwordEvent = CreateEvent(nullptr, FALSE, FALSE, L"PASSWORD");
    HANDLE readyEvent = CreateEvent(nullptr, FALSE, FALSE, L"READY");
    HANDLE readySentEvent = CreateEvent(nullptr, FALSE, FALSE, L"READYSent");
    HANDLE commandEvent = CreateEvent(nullptr, FALSE, FALSE, L"COMMAND");
    HANDLE exitEvent = CreateEvent(nullptr, FALSE, FALSE, L"EXIT");
    HANDLE passwordSentEvent = CreateEvent(nullptr, FALSE, FALSE, L"PASSWORDSent");
    HANDLE breakEvent = CreateEvent(nullptr, FALSE, FALSE, L"BREAK");
    HANDLE disconnectEvent = CreateEvent(nullptr, FALSE, FALSE, L"DISCONNECT");

	HANDLE allEvents[6] = { connectEvent, exitEvent, readySentEvent, passwordSentEvent, breakEvent, disconnectEvent };

	//each event place in the array
	const int connectEventConst = 0;
	const int passwordEventConst = 1;
	const int readyEventConst = 2;
	const int commandEventConst = 3;
	const int exitEventConst = 4;


	int analyzeChannels(char* data);
	int analyzePoints(char* data);
	int addUnitToString(char* pointName);
	int addDateTimeToString();
	int createNewFileName();
	int createNewFile();

public:
        DataReceiverAndAnalyzer(SocketWrapper* inSock, string* logAddress, string *outputString, mutex* stringLock);

	int receiveAndAnalyzeStartingPackets(wchar_t* output);
	int receiveAndAnalyzeDataPackets(char* data);

	string toString();

	int logToFile();

	int display();

	bool isDataReceived();
	bool freeDataReceived();

	int checkState();
	int checkEvent();

	void run();
};

