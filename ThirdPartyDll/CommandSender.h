#pragma once
#include "SocketWrapper.h"
#include <atomic>
#include <mutex>
#include "Command_t.h"


#define COMMAND_MAX_LEN 1000

class CommandSender
{
private:
	enum States
	{
		connectState,
		commandState,
		passwordState,
		readyState,
		disconnectState,
		breakState,
		exitState
	};
	
	States currentState = disconnectState;

	atomic<Command_t>* commandToDo;
	mutex passwordSent;

	SocketWrapper* sock;

	HANDLE connectEvent = CreateEvent(NULL, FALSE, FALSE, L"CONNECTIONS");
	HANDLE passwordEvent = CreateEvent(NULL, FALSE, FALSE, L"PASSWORD");
	HANDLE passwordSentEvent = CreateEvent(NULL, FALSE, FALSE, L"PASSWORDSent");
	HANDLE readyEvent = CreateEvent(NULL, FALSE, FALSE, L"READY");
	HANDLE readySentEvent = CreateEvent(NULL, FALSE, FALSE, L"READYSent");
	HANDLE commandEvent = CreateEvent(NULL, FALSE, FALSE, L"COMMAND");
	HANDLE exitEvent = CreateEvent(NULL, FALSE, FALSE, L"EXIT");
	HANDLE breakEvent = CreateEvent(NULL, FALSE, FALSE, L"BREAK");
	HANDLE disconnectEvent = CreateEvent(NULL, FALSE, FALSE, L"DISCONNECT");

	HANDLE allEvents[5] = { connectEvent, exitEvent, passwordEvent, readyEvent, commandEvent };
	
	//each event place in the array
	const int connectEventConst = 0;
	const int passwordEventConst = 1;
	const int readyEventConst = 2;
	const int commandEventConst = 3;
	const int exitEventConst = 4;

	int createPacket(wchar_t* command, char* packet);
	int sendPacket(char* packet, unsigned int len);
	int checkToSend();

public:
	CommandSender(SocketWrapper* inSock, atomic<Command_t>* command);
	int sendCommand(wchar_t* command);
	
	int checkState();
	int checkEvents();

	int run();
};

