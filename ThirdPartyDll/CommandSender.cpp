#include "CommandSender.h"

using namespace std;

CommandSender::CommandSender(SocketWrapper* inSock, atomic<Command_t>* command)
{
	sock = inSock;
	commandToDo = command;
}

int CommandSender::createPacket(wchar_t* command, char* packet)
{
	//char buff[COMMAND_MAX_LEN];
	int len = wcslen(command) * sizeof(wchar_t) + 6; //4 bytes for int and 2 bytes for empty bytes at the end
	memcpy(packet, &len, sizeof(int));
	memcpy(&packet[4], command, len - 4);
	return len;
}

int CommandSender::sendPacket(char* packet, unsigned int len)
{
	try
	{
		sock->send(packet, len);
	}
	catch (const exception e)
	{
		cout << e.what() << endl;
		exit(1);
	}
	return 0;
}

int CommandSender::sendCommand(wchar_t* command)
{
	char buff[COMMAND_MAX_LEN];
	int len = createPacket(command, buff);
	int result = sendPacket(buff, len);
	
	return result;
}

int CommandSender::checkState()
{
	if (currentState == passwordState)
		cout << "stateChanged" << endl;
	switch (currentState)
	{
	case CommandSender::connectState:
		break;
	case CommandSender::commandState:
		currentState = connectState;
		return checkToSend();
		break;
	case CommandSender::readyState:
		 sendCommand(const_cast<wchar_t*>(L"Ready"));
		 SetEvent(readySentEvent);
		 currentState = connectState;
		break;
	case CommandSender::passwordState:
		sendCommand(const_cast<wchar_t*>(L"coursework"));
		SetEvent(passwordSentEvent);
		currentState = connectState;
		break;
	}

	return 0;
}

int CommandSender::checkEvents()
{
	DWORD eventResult;
	if (currentState != disconnectState)
	{
		eventResult = WaitForMultipleObjects(4, &allEvents[1], FALSE, 10);
		if (eventResult >= WAIT_OBJECT_0 && eventResult < WAIT_OBJECT_0+10)
			cout << "an event received " << eventResult << endl;

		switch (eventResult)
		{
		case (WAIT_OBJECT_0 + 0): //exitEvent
			currentState = exitState;
			break;
		case (WAIT_OBJECT_0 + 1): //passwordEvent
			cout << " password event received" << endl;
			currentState = passwordState;
			return passwordEventConst;
			break;
		case (WAIT_OBJECT_0 + 2): //readyEvent
			if(currentState != breakState)
				currentState = readyState;
			return readyEventConst;
			break;
		case (WAIT_OBJECT_0 + 3): //commandEvent
			currentState = commandState;
			return commandEventConst;
			break;
		
		default:
			break;
		}
	}
	else
	{
		eventResult = WaitForMultipleObjects(2, &allEvents[0], FALSE, 10);
		//cout << "event result = " << eventResult << endl;
		if (eventResult == WAIT_OBJECT_0)
		{
			currentState = connectState;
			ResetEvent(connectEvent);
			cout << "connect event is received in sender";
		}
		else if (eventResult == WAIT_OBJECT_0 + 1) //exitEvent
		{
			currentState = exitState;
			cout << "exit event received in sender" << endl;
		}
			
		
			
	}
	

	return eventResult;
}

int CommandSender::run()
{
	int eventResult = 0;
	while (currentState != exitState)
	{
		checkEvents();
		checkState();
	}
	cout << "exit the sender thread" << endl;
	return 0;
}

int CommandSender::checkToSend()
{
	int result = -1;

	Sleep(10);
	while (!commandToDo->is_lock_free());
	switch (commandToDo->load())
	{
	case freeCommand:
		//cout << "free command" << endl;
		break;
	case startCommand:
		if (sock->isConnected())
		{
			result = sendCommand(const_cast<wchar_t*>(L"Start"));
			cout << "start is sent" << endl;
		}
		else
		{
			result = -1;
		}
		SetEvent(readySentEvent);
		break;
	case stopCommand:
		if (sock->isConnected())
		{
			result = sendCommand(const_cast<wchar_t*>(L"Stop"));
		}
		else
		{
			result = -1;
		}
		
		SetEvent(disconnectEvent);
		currentState = disconnectState;
		break;
	case breakCommand:
		if (sock->isConnected())
		{
			result = sendCommand(const_cast<wchar_t*>(L"Break"));
		}
		else
		{
			result = -1;
		}
		currentState = breakState;
		SetEvent(breakEvent);
		break;
	case passwordCommand:
		if (sock->isConnected())
		{
			result = sendCommand(const_cast<wchar_t*>(L"coursework"));
			cout << "coursework is sent" << endl;
		}
		else
		{
			result = -1;
		}
		break;
	case readyCommand:
		if (sock->isConnected())
		{
			result = sendCommand(const_cast<wchar_t*>(L"Ready"));
		}
		else
		{
			result = -1;
		}
		break;
	default:
		break;
	}
	Sleep(10);
	while (!commandToDo->is_lock_free());
    commandToDo->store(freeCommand);
	return result;
}

