#pragma once
#include <string>
#include <iostream>
#include <atomic>
#include "Command_t.h"
#include "SocketWrapper.h"
#include <Windows.h>
using namespace std;

class KeyboardReceiver
{
private:
	atomic<Command_t> *commandToDo;

	HANDLE connectEvent = CreateEvent(NULL, FALSE, FALSE, L"CONNECTION");
	HANDLE connectsEvent = CreateEvent(NULL, TRUE, FALSE, L"CONNECTIONS");
	HANDLE passwordEvent = CreateEvent(NULL, FALSE, FALSE, L"PASSWORD");
	HANDLE readyEvent = CreateEvent(NULL, FALSE, FALSE, L"READY");
	HANDLE readySentEvent = CreateEvent(NULL, FALSE, FALSE, L"READYSent");
	HANDLE commandEvent = CreateEvent(NULL, FALSE, FALSE, L"COMMAND");
	HANDLE exitEvent = CreateEvent(NULL, TRUE, FALSE, L"EXIT");

	SocketWrapper* sock;
	
	bool connectionStatus = FALSE;

public:
	KeyboardReceiver(SocketWrapper* inSocket, atomic<Command_t>* command);
	void operator()();
};

