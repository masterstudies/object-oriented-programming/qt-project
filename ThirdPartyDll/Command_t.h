#pragma once

enum Command_t {
        freeCommand         = 0,
        startCommand        = 1,
        stopCommand         = 2,
        exitCommand         = 3,
        breakCommand        = 4,
        connectCommand      = 5,
        disconnectCommand   = 6,
        passwordCommand     = 7,
        readyCommand        = 8
};
