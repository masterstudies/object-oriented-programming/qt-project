#include "SocketWrapper.h"

int SocketWrapper::initConnection()
{
	ZeroMemory(&address, sizeof(address));
	address.ai_family = AF_UNSPEC;
	address.ai_socktype = SOCK_STREAM;
	address.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	int iResult = getaddrinfo(ipAddress, portAddress, &address, &resultAddress);
	if (iResult != 0) {
		char ebuff[300];
		sprintf_s(ebuff, SOCKET_MAX_LEN, "getaddrinfo failed: %d\n", iResult);
		WSACleanup();
		throw exception(ebuff);
		return -1;
	}

	connectSocket = INVALID_SOCKET;
	// Create a SOCKET for connecting to server
	connectSocket = socket(resultAddress->ai_family, resultAddress->ai_socktype,
		resultAddress->ai_protocol);

	if (connectSocket == INVALID_SOCKET) {
		char ebuff[300];
		sprintf_s(ebuff, SOCKET_MAX_LEN, "Error at socket(): %ld\n", WSAGetLastError());
		freeaddrinfo(resultAddress);
		WSACleanup();
		throw exception(ebuff);
		return -1;
	}

	DWORD timeout = 10;

	if (setsockopt(connectSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(DWORD)))
	{
		perror("setsockopt");
		//return -1;
	}
	return 0;
}

SocketWrapper::SocketWrapper(const char *ipAddr, const char *port)
{
	ipAddress = (char*)ipAddr;
	portAddress = (char*)port;
	//initial winsock
	int iResult;
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		char ebuff[300];
		sprintf_s(ebuff, SOCKET_MAX_LEN, "WSAStartup failed: %d\n", iResult);
		throw exception(ebuff);
	}

	
}

int SocketWrapper::connect()
{
	lockSocket.lock();
	if(initConnection() < 0)
		return -1;

	// Connect to server.
	int iResult;
	iResult = ::connect(connectSocket, resultAddress->ai_addr, (int)resultAddress->ai_addrlen);
	
	if (connectSocket == INVALID_SOCKET) {
	
		cout << "Unable to connect to server!\n";
		WSACleanup();
		throw exception("Unable to connect to server!\n");
		return -1;
	}
	connection = 1;
	lockSocket.unlock();
	return 0;
}

int SocketWrapper::send(wchar_t* str, UINT16 len)
{
	lockSocket.lock();
	//real length in byte is wchar len * sizeof(wchar_t) + 4 bytes of length in beginig + 2 bytes of zero for end of file
	UINT32 length = len * sizeof(wchar_t) + 6;
	char buff[SOCKET_MAX_LEN];
	buff[0] = (char) length & 0xFF;
	buff[1] = (char)(length >> 8) & 0xFF;
	buff[2] = (char)(length >> 16) & 0xFF;
	buff[3] = (char)(length >> 24) & 0xFF;
	buff[length - 1] = 0;
	buff[length - 2] = 0;

	memcpy(&buff[4], str, length - 6);
	
	int iResult = ::send(connectSocket, buff, length, 0);

	if (iResult == SOCKET_ERROR) {
		char ebuff[300];
		sprintf_s(ebuff, SOCKET_MAX_LEN, "send failed: %d\n", WSAGetLastError());
		WSACleanup();
		throw exception(ebuff);
		//this->lockSocket.unlock();
		return -1;
	}
	lockSocket.unlock();
	return 0;
}

int SocketWrapper::send(char* str, UINT16 len)
{
	lockSocket.lock();
	int iResult = -1;
	if (receiver == 0)
	{
		//this->lockSocket.lock();
		iResult = ::send(connectSocket, str, len, 0);
		if (iResult == SOCKET_ERROR) {
			char ebuff[300];
			sprintf_s(ebuff, SOCKET_MAX_LEN, "send failed: %d\n", WSAGetLastError());

			WSACleanup();
			lockSocket.unlock();
			throw exception(ebuff);
			return -1;
		}
		lockSocket.unlock();
	}
	return iResult;
}

int SocketWrapper::recv(char* str, UINT16 maxLen)
{
	int iResult = -1;
	
	lockSocket.lock(); //wait until sending or other things are in process
	
	iResult = 0;

	iResult = ::recv(connectSocket, str, maxLen * sizeof(wchar_t) + 6, 0);
	setReceiver(0);
	char ebuff[300];
	if (iResult == 0)
	{
		cout << "Connection is closed\n";
		lockSocket.unlock();
		throw exception("Connection is closed\n");
	}
	else if (iResult < 0)
	{
		int err = WSAGetLastError();
		if (err != 10060)
		{
			cout<< "recv failed\n";
		}
		lockSocket.unlock();
		throw exception("recv failed\n");
	}
	//this->lockSocket.unlock();
	lockSocket.unlock();
	return iResult;
}

bool SocketWrapper::isValid()
{
	return (connectSocket != INVALID_SOCKET);
}

bool SocketWrapper::isConnected()
{
	lockSocket.lock();
	int error = 0;
	socklen_t len = sizeof(error);
	int retval = getsockopt(connectSocket, SOL_SOCKET, SO_ERROR, (char*) &error, &len);
	lockSocket.unlock();
	return (retval == 0 && error == 0);
	
}

int SocketWrapper::disconnect()
{
	
	lockSocket.lock();
	int result = closesocket(connectSocket);
	lockSocket.unlock();
	return result;
}

bool SocketWrapper::connectionValid()
{
	return connection;
}

bool SocketWrapper::setReceiver(bool val)
{
	receiver = val;
	return true;
}

bool SocketWrapper::getReceiver()
{
	return receiver;
}

