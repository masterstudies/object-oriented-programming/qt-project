#include "DataReceiverAndAnalyzer.h"

/***************************************************************************************
				Private Fnctions
*****************************************************************************************/
int DataReceiverAndAnalyzer::analyzeChannels(char* data)
{
	addDateTimeToString();
	int pp = 0; //pointer position

	int channels = (int) * (&data[pp]);

	//cout << "number of channels = " << channels << endl << endl;

	pp += sizeof(int);

	for (int ch_num = 0; ch_num < channels; ch_num++)
	{
		//number of points
		int points = (int) * (&data[pp]);
		pp += 4;
		//cout << "number of points = " << points << endl ;

		//channel name
		char* channelName = &data[pp];
		//cout << endl << "channel name= " << channelName << endl;
		pp += strlen(&data[pp]) + 1;

        outputString += string(channelName) + ":\r\n";

		for (int point_num = 0; point_num < points; point_num++)
		{
			int increasePointer = analyzePoints(&data[pp]);
			pp += increasePointer;
		}
        outputString += "\r\n";
	}
	return 0;
}

int DataReceiverAndAnalyzer::analyzePoints(char* data)
{
	int pp = 0, result = -1;
	char* pointName = &data[pp];
	//cout << "point name = " << pointName;
	pp += strlen(&data[pp]) + 1;
	
	outputString += string(pointName) + ": ";

	if (strcmp(pointName, "Solution concentration") == 0 || strcmp(pointName, "Level") == 0)
	{
		int value;
		memcpy(&value, &data[pp], sizeof(int));
		//cout << "\t value= " << value << endl;
		outputString += to_string(value);
		result = pp + sizeof(int);
	}
	else
	{
		double value;
		memcpy(&value, &data[pp], sizeof(double));
		//cout << "\t value= " << value << endl;
		outputString += to_string(value);
		result = pp + sizeof(double);
	}
	addUnitToString(pointName);
    outputString += "\r\n";
	return result;
}

int DataReceiverAndAnalyzer::addUnitToString(char* pointName)
{
	string pointNameStr = string(pointName);

	if (pointNameStr == string("Inside temperature"))
	{
		char buff[3];
		buff[0] = 0xB0; //F8  B0
		buff[1] = 'C';
		buff[2] = 0;

		outputString += buff;
	}
	else if (pointNameStr == string("Air inlet") || pointNameStr == string("Gas inlet") || pointNameStr == string("Gas outlet") || pointNameStr == string("Inlet") || pointNameStr == string("Outlet"))
	{
		char buff[5];
		buff[0] = 'm';
		buff[1] = 0xB3; //FC  B3
		buff[2] = '/';
		buff[3] = 's';
		buff[4] = 0;
		outputString += buff;
	}
	else if (pointNameStr == string("Solution conductivity"))
		outputString += "S/m";
	else if (pointNameStr == string("Solution concentration") || pointNameStr == string("Level"))
		outputString += "%";
	

	return 0;
}

int DataReceiverAndAnalyzer::addDateTimeToString()
{
	SYSTEMTIME t;
	int result;
	char buffer[1024];
	string dateTime = "Measurement results at ";

	GetLocalTime(&t);

	result = GetDateFormatA(LOCALE_USER_DEFAULT,
		0,
		&t,
		(char*)"yyyy-MM-dd",
		(char*)buffer,
		sizeof(buffer));
	if (result > 0)
	{
		dateTime += string(buffer);
	}
	else
	{
		return -1;
	}


	result = GetTimeFormatA(LOCALE_USER_DEFAULT,
		0,
		&t,
        (char*)" hh:mm:ss \r\n",
		(char*)buffer,
		sizeof(buffer));
	if (result > 0)
	{
		dateTime += string(buffer);
	}
	else
	{
		return -1;
	}

	outputString += dateTime;
	return 0;
}

int DataReceiverAndAnalyzer::createNewFileName()
{
	SYSTEMTIME t;
	int result;
	char buffer[1024];
	string dateTime = "LogFile";

	GetLocalTime(&t);

	result = GetDateFormatA(LOCALE_USER_DEFAULT,
		0,
		&t,
		(char*)"yyyy-MM-dd",
		(char*)buffer,
		sizeof(buffer));
	if (result > 0)
	{
		dateTime += string(buffer);
	}
	else
	{
		return -1;
	}


	result = GetTimeFormatA(LOCALE_USER_DEFAULT,
		0,
		&t,
		(char*)"_hh-mm-ss",
		(char*)buffer,
		sizeof(buffer));
	if (result > 0)
	{
		dateTime += string(buffer);
	}
	else
	{
		return -1;
	}

	logFileName = dateTime + string(".txt");
	return 0;
}

int DataReceiverAndAnalyzer::createNewFile()
{
		createNewFileName();
        logCompeleteAddress = *logAddress + string("\\") + logFileName;
        if (*logAddress != "")
		{
            int numberOfFolders = count(logAddress->begin(), logAddress->end(), '\\');
            int lastSlash = logAddress->find('\\');
			for (int i = 0; i < numberOfFolders; i++)
			{
                string logAddressSub = logAddress->substr(0, lastSlash);
				int r = CreateDirectoryA(logAddressSub.c_str(), NULL);
                lastSlash = logAddress->find('\\', lastSlash + 1);
				//cout << numberOfFolders << logAddressSub << endl;
			}

            int r = CreateDirectoryA(logAddress->c_str(), NULL);

			if (GetLastError() != ERROR_SUCCESS && GetLastError() != ERROR_ALREADY_EXISTS)
				return -1;
		}

		logFile.open(logCompeleteAddress, fstream::out | fstream::app);
		if (!logFile.is_open())
			return -1;
		logFile.close();

	return 0;
}

/***************************************************************************************
				Public Fnctions
*****************************************************************************************/
DataReceiverAndAnalyzer::DataReceiverAndAnalyzer(SocketWrapper* inSock, string* logAddress, string *outputString, mutex* stringLock)
{
	sock = inSock;
	this->logAddress = logAddress;
    outputStringPtr = outputString;
    outputLock = stringLock;
}

int DataReceiverAndAnalyzer::receiveAndAnalyzeStartingPackets(wchar_t* output)
{
	receivedData.store(0);
	int len = -1;
	char buffer[ANALYZE_MAX_LEN];
	if (sock->isConnected())
	{
		try
		{
			len = sock->recv(buffer, ANALYZE_MAX_LEN);
		}
		catch (const exception & e) {
			//cout << e.what() << endl;
			return len;
		}

		memcpy(output, &buffer[4], len - 4);
	}
	receivedData.store(1);
	//output = (wchar_t*) &buffer[4];
	return (int) *(&buffer[0]);
}

int DataReceiverAndAnalyzer::receiveAndAnalyzeDataPackets(char* data)
{
	outputString = "";
	int len = -1;
	char buffer[ANALYZE_MAX_LEN];
	try
	{
		len = sock->recv(buffer, ANALYZE_MAX_LEN);
	}
	catch (const exception & e) {
		//cout << e.what() << endl;
		return len;
	}

	int length;
	memcpy(&length, &buffer[0], 4);
	//cout << "length = " << length << "   " << len << endl;

	analyzeChannels(&buffer[4]);
	display();

	logToFile();

	return len;
}

string DataReceiverAndAnalyzer::toString()
{
    outputLock->lock();
    *outputStringPtr += outputString;
    outputLock->unlock();
	return outputString;
}

int DataReceiverAndAnalyzer::logToFile()
{

	logFile.open(logCompeleteAddress, fstream::out | fstream::app);
	if (!logFile.is_open())
	{
        cout << "\r\n\r\n\r\nfile is not open " << GetLastError() << endl;
		exit(1);
	}
	
	logFile << toString();
	logFile.close();
	outputString = "";

	return 0;
}

int DataReceiverAndAnalyzer::display()
{
	string displayString = toString();
	std::replace(displayString.begin(), displayString.end(), (char)0xB3, (char)0xFC); //change cube symbol to show
	std::replace(displayString.begin(), displayString.end(), (char)0xB0, (char)0xF8); //change degree symbol to show
	
	cout << displayString;
	
	return 0;
}

bool DataReceiverAndAnalyzer::isDataReceived()
{
	return receivedData.load();
}

bool DataReceiverAndAnalyzer::freeDataReceived()
{
	receivedData.store(0);
	return false;
}

int DataReceiverAndAnalyzer::checkState()
{
	wchar_t wbuffer[ANALYZE_MAX_LEN];
	char buffer[ANALYZE_MAX_LEN];
	switch (currentState)
	{
	case DataReceiverAndAnalyzer::initialState:
		if (receiveAndAnalyzeStartingPackets(wbuffer) >= 0)
		{
			wcout << wbuffer << endl;
			if (wcscmp(wbuffer, L"Identify") == 0)
			{
				SetEvent(passwordEvent);
				cout << "password event is set" << endl;
			}

            int result = createNewFile();
            if (result < 0)
            {
                cout << "There is an error with log file address. INVALID_NAME or there is no permission" << endl;
                exit(1);
            }
			currentState = idleState;
		}
		break;
	case DataReceiverAndAnalyzer::readyState:
		//cout << "readyState" << endl;
		if (receiveAndAnalyzeDataPackets(buffer) > 0)
		{
			logToFile();
			currentState = idleState;
			SetEvent(readyEvent);
		}
		break;
	case DataReceiverAndAnalyzer::disconnectState:
		break;
	default:
		break;
	}
	return 0;
}

int DataReceiverAndAnalyzer::checkEvent()
{
	DWORD eventResult;
	
	if (currentState != disconnectState)
	{
		eventResult = WaitForMultipleObjects(5, &allEvents[1], FALSE, 10);

		if (eventResult == WAIT_OBJECT_0 + 4)
			cout << "disconnect event received" << endl;

		switch (eventResult)
		{
		//case (WAIT_OBJECT_0): //connectEvent
		//	currentState = initialState;
		//	return connectEventConst;
		//	break;
		case (WAIT_OBJECT_0 + 0): //exitEvent
			currentState = exitState;
			return exitEventConst;
			break;
		case (WAIT_OBJECT_0 + 1): //readySentEvent
			currentState = readyState;
			return readyEventConst;
			break;
		case (WAIT_OBJECT_0 + 2): //passwordSentEvent
			currentState = initialState;
			return 4;
			break;
		case (WAIT_OBJECT_0 + 3): //breakEvent
			currentState = idleState;
			return 5;
			break;
		case (WAIT_OBJECT_0 + 4): //disconnectEvent
			cout << "changed to disconnect state" << endl; 
			currentState = disconnectState;
			return 6;
			break;
		default:
			break;
		}
	}
	else
	{
		eventResult = WaitForMultipleObjects(2, allEvents, FALSE, 10);
		if (eventResult == WAIT_OBJECT_0)
		{
			currentState = initialState;
			ResetEvent(connectEvent);
		}
		else if (eventResult == WAIT_OBJECT_0 + 1)//exitEvent
		{
			currentState = exitState;
			return exitEventConst;
		}

	}
	return eventResult;
}

void DataReceiverAndAnalyzer::run()
{
	while (currentState != exitState)
	{
		checkEvent();
		checkState();
	}
	cout << "exit the receiver thread" << endl;
}

