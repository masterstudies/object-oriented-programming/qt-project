#pragma once
#pragma comment(lib, "Ws2_32.lib")

#include <iostream>
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <string>
#include <wchar.h>
#include <exception>
#include <stdio.h>
#include <mutex>

#define SOCKET_MAX_LEN 2048

using namespace std;

//A socket object which is able to send and receive wchar_t with adding data length and end chars
class SocketWrapper
{
//public:
private:
	WSADATA wsaData;
	struct addrinfo address, *resultAddress;
	SOCKET connectSocket;
	mutex lockSocket;
	bool connection = 0;
	bool receiver = 1;

	char *ipAddress, *portAddress;

	int initConnection();
	

public:
	SocketWrapper(const char* ipAddr, const char* port);
	int connect();
	int send(wchar_t* str, UINT16 len);
	int send(char* str, UINT16 len);
	int recv(char* str, UINT16 maxLen);
	bool isValid();
	bool isConnected();
	int disconnect();

	bool connectionValid();
	bool setReceiver(bool val);
	bool getReceiver();


};
