#pragma once
#include <string>

using namespace std;

class DataLogger
{
private:
	string fileAddress;
	string* dataToLog;

	int createFile();
	int openFile();


public:
	DataLogger(string fileName, string* dataToLog);
	int logToFile();
};

