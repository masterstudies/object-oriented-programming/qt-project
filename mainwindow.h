#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QMessageBox>
#include <QThread>
#include "IAS0410ObjPlantLogger.h"
#include "logger.h"
#include <QCloseEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(SocketWrapper* inSocket, atomic<Command_t> *command, string* fileAddress,
               string* outputString, mutex* outputLock, QWidget *parent = nullptr);
    ~MainWindow();

private slots:


    void on_OpenPushButton_clicked();

    void on_ClosePushButton_clicked();

    void on_ConnectPushButton_clicked();

    void on_ExitPushButton_clicked();

    void on_DisconnectPushButton_clicked();

    void on_StartPushButton_clicked();

    void on_BreakPushButton_clicked();

    void on_MainWindow_destroyed();

    void on_LogbookTextBrowser_textChanged();

    void MainWindow::closeEvent (QCloseEvent *event) override;

private:
    Ui::MainWindow *ui;

    MainWindow *MyMW;

    atomic<Command_t> *commandToDo = nullptr;

    SocketWrapper* sock;

    bool connectionStatus = FALSE;

    string* fileAddress;
    string* outputString;

    mutex* outputLock;

    HANDLE connectEvent = CreateEvent(nullptr, FALSE, FALSE, L"CONNECTION");
    HANDLE connectsEvent = CreateEvent(nullptr, TRUE, FALSE, L"CONNECTIONS");
    HANDLE passwordEvent = CreateEvent(nullptr, FALSE, FALSE, L"PASSWORD");
    HANDLE readyEvent = CreateEvent(nullptr, FALSE, FALSE, L"READY");
    HANDLE readySentEvent = CreateEvent(nullptr, FALSE, FALSE, L"READYSent");
    HANDLE commandEvent = CreateEvent(nullptr, FALSE, FALSE, L"COMMAND");
    HANDLE exitEvent = CreateEvent(nullptr, TRUE, FALSE, L"EXIT");

    Logger *logger = nullptr; // = new Logger(outputString, outputLock);

    bool closed = false;
    void run();
};
#endif // MAINWINDOW_H
